module.exports = {
  root: true,
  extends: '@react-native-community',
  rules: {
    'prettier/prettier': [
      'error', {
        'singleQuote': true,
        'trailingComma': 'es5',
        'printWidth': 120,
        'jsxBracketSameLine': true, // disable extra line for > bracket
        // 'semi': false, // disable semicolons
      },
    ],
    curly: ['error', 'multi-line'],
    'arrow-body-style': ['error', 'as-needed'],
    'no-var': 1,
    'prefer-const': ['error', {
      'destructuring': 'any',
      'ignoreReadBeforeAssign': false,
    }],
    'no-redeclare': ['error'], // "extends": "eslint:recommended"
    'no-duplicate-imports': ['error'],
    'react-native/no-unused-styles': 1,
    'react-native/no-color-literals': 1,
  },
};
