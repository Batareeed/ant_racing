import React, { Component, Fragment } from 'react';
import { ActivityIndicator, FlatList, SafeAreaView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { connect } from 'react-redux';
import { antCalculate, antCalculateAll, antListLoad } from '../redux/actions/antsActions';
import AntView from './AntView';

class MainScreen extends Component {
  componentDidMount() {
    this.props.dispatch(antListLoad());
  }

  renderLoading = () => (
    <View style={styles.status}>
      <ActivityIndicator />
    </View>
  );

  renderError = () => {
    const { dispatch, error } = this.props;
    return (
      <TouchableOpacity style={styles.status} onPress={() => dispatch(antListLoad())}>
        <Text>{(error || 'Error').toString()}</Text>
      </TouchableOpacity>
    );
  };

  renderData = () => {
    const { list, ants, dispatch } = this.props;
    return (
      <Fragment>
        <FlatList
          style={styles.list}
          data={list}
          renderItem={({ item }) => <AntView ant={ants[item]} onPress={() => dispatch(antCalculate(item))} />}
          keyExtractor={item => item.toString()}
          ItemSeparatorComponent={({ sectionID, rowID }) => (
            <View style={styles.separator} key={{ sectionID } - { rowID }} />
          )}
        />
        <TouchableOpacity style={styles.button} onPress={() => dispatch(antCalculateAll())}>
          <Text>Calculate all</Text>
        </TouchableOpacity>
      </Fragment>
    );
  };

  render() {
    const { isLoading, error } = this.props;
    let content;
    if (isLoading) content = this.renderLoading();
    else if (error) content = this.renderError();
    else content = this.renderData();
    return <SafeAreaView style={styles.safeArea}>{content}</SafeAreaView>;
  }
}

const mapStateToProps = state => {
  const { list, ants, error, isLoading } = state.antsReducer;
  return { list, ants, error, isLoading };
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    alignItems: 'stretch',
  },
  list: {
    flex: 1,
  },
  status: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 16,
  },
  separator: {
    height: StyleSheet.hairlineWidth,
    alignSelf: 'stretch',
    backgroundColor: '#bbb',
    flex: 1,
  },
  button: {
    height: 40,
    marginHorizontal: 32,
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    borderWidth: 2,
    borderColor: 'black',
  },
});

export default connect(mapStateToProps)(MainScreen);
