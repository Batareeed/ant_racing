import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity, ActivityIndicator } from 'react-native';
import { ANT_BASE_WIDTH } from '../constants';

const Ant = ({ ant: { size: { width, height } = {}, color = 'black' } }) => {
  const backgroundColor = color.toLowerCase();
  const borderRadius = height / 2;
  return <View style={{ backgroundColor, width, borderRadius }} />;
};

const Trace = ({ style, ant: { size: { width, height } = {}, color = 'black' } }) => {
  const backgroundColor = color.toLowerCase();
  return <View style={[styles.trace, { backgroundColor, width, height }, style]} />;
};

const Track = ({ ant, ant: { size: { width, height } = {}, score } }) => (
  <View style={[styles.track, { height }]}>
    {score ? (
      <View style={[styles.traceContainer, { flex: score }]}>
        <Trace ant={ant} style={{ marginRight: -width / 2 }} />
        <Ant ant={ant} />
      </View>
    ) : (
      <Ant ant={ant} />
    )}
  </View>
);

const AntView = ({ ant, ant: { name, isCalculating, score }, onPress }) => {
  const Container = isCalculating ? View : TouchableOpacity;
  let right;
  if (isCalculating) right = <ActivityIndicator />;
  else if (score) right = <Text style={styles.score}>{score.toFixed(2)}</Text>;
  else right = <Text style={styles.calculate}>Calculate</Text>;

  return (
    <Container style={styles.container} onPress={onPress}>
      <View style={styles.left} onPress={onPress}>
        <Text style={styles.name}>{name}</Text>
        <Track ant={ant} />
      </View>
      <View style={styles.right}>{right}</View>
    </Container>
  );
};

export default AntView;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
  left: {
    flex: 1,
  },
  name: {
    flex: 1,
    fontSize: 12,
    marginBottom: 8,
  },

  track: {
    alignSelf: 'stretch',
    alignItems: 'stretch',
    flexDirection: 'row',
  },
  traceContainer: {
    flexDirection: 'row',
    alignItems: 'stretch',
    paddingLeft: ANT_BASE_WIDTH,
  },
  trace: {
    flex: 1,
    opacity: 0.3,
    marginLeft: -ANT_BASE_WIDTH,
  },
  right: {
    width: 80,
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  score: {
    fontSize: 14,
  },
  calculate: {
    fontSize: 14,
    color: 'gray',
  },
});
