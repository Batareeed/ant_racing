export function deepClone(oldObj) {
  let newObj = oldObj;
  if (oldObj && typeof oldObj === 'object') {
    newObj = Object.prototype.toString.call(oldObj) === '[object Array]' ? [] : {};
    for (const i in oldObj) {
      newObj[i] = deepClone(oldObj[i]);
    }
  }
  return newObj;
}
