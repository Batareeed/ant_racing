import {
  ANT_CALCULATE,
  ANT_CALCULATE_ALL,
  ANT_CALCULATING,
  ANT_CALCULATING_RESULT,
  ANT_LIST_LOAD,
  ANT_LIST_LOADING,
  ANT_LIST_LOADING_RESULT,
} from '../constants';

export const antListLoad = () => ({
  type: ANT_LIST_LOAD,
});

export const antListLoading = (isLoading = true) => ({
  type: ANT_LIST_LOADING,
  payload: { isLoading },
});

export const antListLoadingResult = ({ ants, error }) => ({
  type: ANT_LIST_LOADING_RESULT,
  payload: { ants, error },
});

export const antCalculate = id => ({
  type: ANT_CALCULATE,
  payload: { id },
});

export const antCalculating = (id, isCalculating = true) => ({
  type: ANT_CALCULATING,
  payload: { id, isCalculating },
});

export const antCalculatingResult = (id, score) => ({
  type: ANT_CALCULATING_RESULT,
  payload: { id, score },
});

export const antCalculateAll = () => ({
  type: ANT_CALCULATE_ALL,
  payload: {},
});
