import { combineReducers } from 'redux';

import antsReducer from './antsReducer';

const appReducer = combineReducers({
  antsReducer,
});

export default appReducer;
