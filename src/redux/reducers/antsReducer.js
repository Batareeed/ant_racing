import { ANT_CALCULATING, ANT_CALCULATING_RESULT, ANT_LIST_LOADING, ANT_LIST_LOADING_RESULT } from '../constants';
import { deepClone } from '../../utils';

const initialState = {
  list: [],
  ants: {},
  error: undefined,
  isLoading: false,
};

export default (state = initialState, action) => {
  const newState = deepClone(state);
  const { payload } = action;
  switch (action.type) {
    case ANT_LIST_LOADING:
      if (payload.isLoading) newState.error = undefined;
      newState.isLoading = payload.isLoading;
      break;
    case ANT_LIST_LOADING_RESULT:
      newState.ants = payload.ants;
      newState.list = payload.ants ? Array.from(Array(Object.keys(payload.ants).length).keys()) : [];
      newState.error = payload.error;
      break;
    case ANT_CALCULATING:
      {
        const { id, isCalculating } = payload;
        newState.ants[id].isCalculating = isCalculating;
      }
      break;
    case ANT_CALCULATING_RESULT:
      {
        const { id, score } = payload;
        newState.ants[id].score = score;
        newState.list.sort((a, b) => (newState.ants[b].score || 0) - (newState.ants[a].score || 0));
      }
      break;
    default:
      return state;
  }
  return newState;
};
