import { all, fork } from 'redux-saga/effects';
import antsSaga from './antsSaga';

export default function* rootSaga() {
  yield all([fork(antsSaga)]);
}
