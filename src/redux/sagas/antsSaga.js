import { all, delay, put, call, takeEvery, takeLeading } from 'redux-saga/effects';
import { ANT_CALCULATE, ANT_CALCULATE_ALL, ANT_LIST_LOAD } from '../constants';
import { antCalculating, antCalculatingResult, antListLoading, antListLoadingResult } from '../actions/antsActions';
import { ANT_BASE_HEIGHT, ANT_BASE_WIDTH } from '../../constants';
import { store } from '../store';

function* requestList() {
  const params = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ query: 'query{ ants { name length color weight}}' }),
  };
  const result = yield fetch('https://antserver-blocjgjbpw.now.sh/graphql', params);
  const json = yield result.json();
  return json.data.ants;
}

export function* load() {
  yield put(antListLoading());
  try {
    const list = yield requestList();
    let maxWidth = 0;
    let maxHeight = 0;
    list.forEach(({ length, weight }) => {
      if (length > maxWidth) maxWidth = length;
      if (weight / length > maxHeight) maxHeight = weight / length;
    });
    const sizedList = list.map(item => {
      const { length, weight } = item;
      const width = (length / maxWidth) * ANT_BASE_WIDTH;
      const height = (weight / length / maxHeight) * ANT_BASE_HEIGHT;
      return { ...item, size: { width, height } };
    });
    yield put(antListLoadingResult({ ants: Object.assign({}, sizedList) }));
  } catch (error) {
    yield put(antListLoadingResult({ error: error.message }));
  }
  yield put(antListLoading(false));
}

function* calculate(id) {
  yield put(antCalculating(id));
  yield delay(7000 + Math.random() * 7000);
  const likelihoodOfAntWinning = Math.random();
  yield put(antCalculatingResult(id, likelihoodOfAntWinning));
  yield put(antCalculating(id, false));
}

export function* calculateOne(action) {
  const { id } = action.payload;
  yield calculate(id);
}

export function* calculateAll() {
  const { list, ants } = store.getState().antsReducer;
  yield all(list.filter(id => !ants[id].isCalculating).map(id => call(() => calculate(id))));
}

export default function* rootSaga() {
  yield all([
    takeEvery(ANT_LIST_LOAD, load),
    takeEvery(ANT_CALCULATE, calculateOne),
    takeLeading(ANT_CALCULATE_ALL, calculateAll),
  ]);
}
